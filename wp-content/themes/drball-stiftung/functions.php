<?php
include 'includes/front/enqueue.php';
add_action('wp_enqueue_scripts', 'drball_enqueue_scripts');

//Post-Thumbnails aktivieren, da diese als Header-Image auf den Seiten verwendet werden
add_theme_support( 'post-thumbnails', array( 'page', 'post' ) );

function register_my_menu() {
    register_nav_menu('main-menu',__( 'Hauptmenü' ));
    register_nav_menu('bottom-menu',__( 'Menü unten' ));
}
add_action( 'init', 'register_my_menu' );