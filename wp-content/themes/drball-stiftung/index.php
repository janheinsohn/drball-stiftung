<?php get_header(); ?>

<?php
$url = get_the_post_thumbnail_url();

?>


<h1>WORK IN PROGRESS</h1>

<main>

    <div id="header">
        <div id="topnavi"><span><a href="https://www.drball-stiftung.de/gaeste-buch/">Gästebuch</a> | <a href="https://www.drball-stiftung.de/kontakt-impressum/">Kontakt</a></span></div>
        <img class="logo" src="<?=get_template_directory_uri();?>/assets/img/dr-ball-stiftung.png" alt="">
    </div>

        <div id="content">
            <header>
                <nav>
                    <!--                        TODO: Hier kommt das NAV hin-->
                    <?php
                    if(has_nav_menu("main-menu")) {
                        wp_nav_menu(  array(
                            'container'       => 'div',
                            'container_id'    => 'navi',
                            'menu_id'         => 'menu',
                            'echo'            => true,
                            'fallback_cb'     => 'wp_page_menu',
                            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                            'item_spacing'    => 'preserve',
                            'depth'           => 0,
                            'theme_location'  => '',
                        ));
                    }
                    ?>
                </nav>
            </header>

            <div class="grafik"><img src="<?=$url?>" alt="Kinder haben ein recht auf seelische Unversehrtheit und ein Recht auf Mutter und Vater."></div>



            <div id="texts">
                <?php
                while(have_posts()) {
                    the_post();
                    the_content();
                } ?>




            </div>

            <div style="clear: both;"></div>
        </div>

        <div id="sidebar">

            <?php

            $args = array(
                'posts_per_page' => 5,
                'order' => 'ASC'
            );

            $custom_query = new WP_Query($args);

            if ($custom_query->have_posts()) : while($custom_query->have_posts()) : $custom_query->the_post(); ?>
                <div class="box">
                    <div class="box_titel"><h1><?php the_title(); ?></h1></div>
                    <div id="tbox0_short" style="padding:0 0 10px 0;">
                        <p>
                            <?php  the_content();?><span class="dots">...</span></p>
                        <div class="box_weiter" ><a href="<?php the_permalink(); ?>">weiterlesen</a></div>
                    </div>
                </div>


                <?php  get_the_excerpt();?>
            <?php endwhile; else : ?>
                <p>Keine Beiträge</p>
            <?php endif; wp_reset_postdata(); ?>

        </div>

</main>

<?php get_footer(); ?>
