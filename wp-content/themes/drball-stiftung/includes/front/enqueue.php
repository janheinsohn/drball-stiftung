<?php
function drball_enqueue_scripts()
{
    wp_register_style('drball_basic_css', get_template_directory_uri() . '/assets/css/basic.css');

    wp_enqueue_style('drball_basic_css');
}